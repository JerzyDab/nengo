import numpy as np
import matplotlib.pyplot as plt
import scipy.io
import os


array_list = []
path_list = []
path = r'processed_data0/scale16/'
folder = os.listdir(path)

for file in folder:
    path_file = path + file
    path_list.append(path_file)
    zeros_array = np.zeros([128, 128], dtype=int)
    image = scipy.io.loadmat(path_file)
    arr = image['dd']

    for event in arr:
        zeros_array[event[3], event[4]] = 1
    array_list.append(zeros_array)


for idx, array in enumerate(array_list):
    plt.imshow(array)
    plt.title(f'{path_list[idx]}')
    plt.show()




